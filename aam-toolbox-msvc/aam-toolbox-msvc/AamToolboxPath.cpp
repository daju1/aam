#include <stdlib.h>
#include <iostream>

std::string AamToolboxPath;
void initAamToolboxPath()
{
	const char * aam_toolbox_path = getenv("AAM_TOOLBOX_PATH");
	if (!aam_toolbox_path)
	{
		printf("Error: you must set enveroment variable AAM_TOOLBOX_PATH");
		return;
	}

	AamToolboxPath = aam_toolbox_path;
	if (!(AamToolboxPath.back() == '/' || AamToolboxPath.back() == '\\'))
	{
		AamToolboxPath += '/';
	}
}