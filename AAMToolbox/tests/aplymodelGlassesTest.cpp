/* Active Appearance Model toolbox
 * Copyright (C) 2012 Ivan Gubochkin
 * e-mail: jhng@yandex.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   aplymodelTest.cpp
 * Author: Иван Губочкин
 *
 * Created on 10.09.2012, 15:00:26
 */

#include <stdlib.h>
#include <iostream>
#include <exception>
#include <opencv2/highgui/highgui.hpp>
#include <boost/assign.hpp>
#include <iterator>

#include "aam/AAMFunctions2D.h"
#include "aam/AAMModel.h"
#include "aam/AAMEstimator.h"


#include "DirectoryEnumerator.h"
extern std::string AamToolboxPath;
extern void initAamToolboxPath();


bool getCenterPoint(aam::Point2D& cp, cv::Mat& im, cv::Rect faceRect)
{
	cv::Mat face_im = im(faceRect);

	cv::CascadeClassifier leye_cascade(AamToolboxPath + "data/haarcascade_mcs_lefteye.xml");
	cv::CascadeClassifier reye_cascade(AamToolboxPath + "data/haarcascade_mcs_righteye.xml");
	if (leye_cascade.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load leye_cascade haar cascade" << std::endl;
		return false;
	}
	if (reye_cascade.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load reye_cascade haar cascade" << std::endl;
		return false;
	}

	std::vector<cv::Rect> leyes;
	leye_cascade.detectMultiScale(face_im, leyes,
		1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		| CV_HAAR_SCALE_IMAGE
		,
		cv::Size(15, 15));

	std::vector<cv::Rect> reyes;
	reye_cascade.detectMultiScale(face_im, reyes,
		1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		| CV_HAAR_SCALE_IMAGE
		,
		cv::Size(15, 15));

	if (leyes.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No leyes on test picture" << std::endl;
		return false;
	}
	if (reyes.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No faces on test picture" << std::endl;
		return false;
	}

	cv::Rect leyeRect = leyes[0];
	cv::Rect reyeRect = reyes[0];

	aam::Point2D cp_leye;
	cp_leye.x = cvRound((faceRect.x + leyeRect.x + leyeRect.width * 0.5));
	cp_leye.y = cvRound((faceRect.y + leyeRect.y + leyeRect.height * 0.5));

	aam::Point2D cp_reye;
	cp_reye.x = cvRound((faceRect.x + reyeRect.x + reyeRect.width * 0.5));
	cp_reye.y = cvRound((faceRect.y + reyeRect.y + reyeRect.height * 0.5));

	cp.x = (cp_leye.x + cp_reye.x) / 2;
	cp.y = (cp_leye.y + cp_reye.y) / 2;

	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x + leyeRect.x, faceRect.y + leyeRect.y), cv::Size(leyeRect.width, leyeRect.height)), CV_RGB(255, 255, 255));
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x + reyeRect.x, faceRect.y + reyeRect.y), cv::Size(reyeRect.width, reyeRect.height)), CV_RGB(255, 255, 255));

	cv::circle(im, cp_leye, 3, CV_RGB(255, 255, 255));
	cv::circle(im, cp_reye, 3, CV_RGB(255, 255, 255));
	cv::circle(im, cp, 3, CV_RGB(255, 255, 255));
	cv::imshow("tmp", im);
	cv::waitKey(0);

	return true;
}


/*
 * Simple C++ Test Suite
 */

void testApplyGray(std::string& fn)
{
    std::cout << "aplymodelTest testAplyGray" << std::endl;

    try
    {
        aam::AAMEstimator estimator;
		//estimator.load(AamToolboxPath + "data/aam_glasses_test.xml");
		estimator.load(AamToolboxPath + "data/aam_frame_glasses_test.xml");

		cv::CascadeClassifier face_cascade(AamToolboxPath + "data/haarcascade_frontalface_alt.xml");

		if (face_cascade.empty())
        {
            std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load haar cascade" << std::endl;
            return;
        }

        //model.load("data/aam_test.xml");

		cv::Mat im = cv::imread(fn);

        if (im.empty())
        {
            std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't read test image" << std::endl;
            return;
        }

        cv::Mat gray;
        cv::cvtColor(im, im, CV_BGR2GRAY);

        std::vector<cv::Rect> faces;
		face_cascade.detectMultiScale(im, faces,
            1.1, 2, 0
            |CV_HAAR_FIND_BIGGEST_OBJECT
            //|CV_HAAR_DO_ROUGH_SEARCH
            |CV_HAAR_SCALE_IMAGE
            ,
            cv::Size(30, 30) );

        if (faces.empty())
        {
            std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No faces on test picture" << std::endl;
            return;
        }

        cv::Rect faceRect = faces[0];

		aam::Point2D cp;
		getCenterPoint(cp, im, faceRect);
        
        //cp.x = cvRound(
        //        (faceRect.x + faceRect.width * 0.5) + 10);
        //cp.y = cvRound(
        //        (faceRect.y + faceRect.height * 0.5) + 30);

        aam::Vertices2DList pos;
        aam::TrainOptions options = estimator.getTrainOptions();
        options.setMaxIters(30);
        estimator.setTrainOptions(options);
        
        uint64 start = cv::getTickCount();
        estimator.estimateAAM(im, cp, pos, true);

        std::cout << "Wo rking time " << (cv::getTickCount() -
                start) / cv::getTickFrequency() << std::endl;

        for (int i = 0; i < pos.size(); i++)
        {
            cv::circle(im, pos[i], 3, CV_RGB(255, 255, 255));
        }
        cv::imshow("result", im);
        cv::waitKey(0);
    }
    catch (std::exception& e)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Exception occured: " <<
                e.what() << std::endl;
    }
}

void testApplyColor(std::string& fn)
{
    std::cout << "aplymodelTest testAplyColor" << std::endl;

    try
    {
        aam::AAMEstimator estimator;
		//estimator.load(AamToolboxPath + "data/aam_glasses_c_test.xml");
		estimator.load(AamToolboxPath + "data/aam_frame_glasses_c_test.xml");

		cv::CascadeClassifier cascade(AamToolboxPath + "data/haarcascade_frontalface_alt.xml");

        if (cascade.empty())
        {
            std::cout << "%TEST_FAILED% time=0 testname=testAplyColor (aplymodelTest) message=Can't load haar cascade" << std::endl;
            return;
        }

        //cv::Mat im = cv::imread("data/IMM/01-1m.jpg");
		cv::Mat im = cv::imread(fn);

        if (im.empty())
        {
            std::cout << "%TEST_FAILED% time=0 testname=testAplyColor (aplymodelTest) message=Can't read test image" << std::endl;
            return;
        }

        std::vector<cv::Rect> faces;
        cascade.detectMultiScale(im, faces,
            1.1, 2, 0
            |CV_HAAR_FIND_BIGGEST_OBJECT
            //|CV_HAAR_DO_ROUGH_SEARCH
            |CV_HAAR_SCALE_IMAGE
            ,
            cv::Size(30, 30) );

        if (faces.empty())
        {
            std::cout << "%TEST_FAILED% time=0 testname=testAplyColor (aplymodelTest) message=No faces on test picture" << std::endl;
            return;
        }

        cv::Rect faceRect = faces[0];

        aam::Point2D cp;
		getCenterPoint(cp, im, faceRect);
		//cp.x = cvRound(
        //        (faceRect.x + faceRect.width * 0.5)  + 10);
        //cp.y = cvRound(
        //        (faceRect.y + faceRect.height * 0.5)  + 30);

        aam::Vertices2DList pos;
        aam::TrainOptions options = estimator.getTrainOptions();
        options.setMaxIters(30);
        estimator.setTrainOptions(options);

        uint64 start = cv::getTickCount();
        estimator.estimateAAM(im, cp, pos, true);

        std::cout << "Working time " << (cv::getTickCount() -
                start) / cv::getTickFrequency() << std::endl;

        for (int i = 0; i < pos.size(); i++)
        {
            cv::circle(im, pos[i], 3, CV_RGB(255, 255, 255));
        }
        cv::imshow("result", im);
        cv::waitKey(0);
    }
    catch (std::exception& e)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testAplyColor (aplymodelTest) message=Exception occured: " <<
                e.what() << std::endl;
    }
}


class AamApplyGrayDirectoryEnumerator
	: public DirectoryEnumerator
{
public:

	AamApplyGrayDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		testApplyGray(fn);
	}
};


class AamApplyColorDirectoryEnumerator
	: public DirectoryEnumerator
{
public:

	AamApplyColorDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		testApplyColor(fn);
	}
};



int main(int argc, char** argv)
{
	initAamToolboxPath();

	if (0)
	{
		AamApplyGrayDirectoryEnumerator work_enumerator("*", "bmp");
		work_enumerator.enumerate("D:/cpplibs/aam/stasm_work/tasm/shapes/glasses");
	}

	if (1)
	{
		AamApplyGrayDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.enumerate("D:/_C++/recognition/inframes");
	}	
	
	if (1)
	{
		AamApplyColorDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.enumerate("D:/_C++/recognition/inframes");
	}



	std::string fn = AamToolboxPath + "data/cootes/107_0766.bmp";

    std::cout << "%SUITE_STARTING% aplymodelTest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% testAplyGray (aplymodelTest)" << std::endl;
    testApplyGray(fn);
    std::cout << "%TEST_FINISHED% time=0 testAplyGray (aplymodelTest)" << std::endl;

    std::cout << "%TEST_STARTED% testAplyColor (aplymodelTest)" << std::endl;
    testApplyColor(fn);
    std::cout << "%TEST_FINISHED% time=0 testAplyColor (aplymodelTest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}
