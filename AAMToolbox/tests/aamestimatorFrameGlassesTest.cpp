/* Active Appearance Model toolbox
 * Copyright (C) 2012 Ivan Gubochkin
 * e-mail: jhng@yandex.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   aamestimatorTest.cpp
 * Author: Иван Губочкин
 *
 * Created on 10.09.2012, 16:16:54
 */

#include <stdlib.h>
#include <iostream>
#include <exception>
#include <iosfwd>
#include <string>
#include <boost/spirit/include/classic.hpp>
#include <vector>
#include <fstream>

#include "aam/AAMEstimator.h"

extern std::string AamToolboxPath;
extern void initAamToolboxPath();


using namespace BOOST_SPIRIT_CLASSIC_NS;

#define TRIANGLES_COUNT         103

int trianglesData[] =
{
	25, 76, 52,
	25, 76, 101,
	51, 52, 76,
	101, 77, 76,
	101, 27, 77,
	52, 1, 51,
	1, 0, 51,
	27, 26, 77,
	0, 51, 75,
	0, 75, 24,
	26, 78, 77,
	26, 78, 50,
	50, 79, 78,
	50, 79, 49,
	49, 80, 79,
	49, 80, 48,
	48, 81, 80,
	48, 81, 47,
	46, 81, 82,
	45, 82, 46,
	45, 82, 83, 
	44, 83, 45,
	44, 83, 84,
	43, 84, 44,
	43, 84, 85,
	42, 85, 43,
	42, 86, 85,
	42, 86, 41,
	41, 86, 87,
	41, 87, 88, 
	41, 88, 40,
	40, 88, 89,
	40, 89, 39,
	39, 90, 89, 
	39, 90, 38,
	38, 91, 90, 
	38, 91, 37,
	37, 92, 91,
	37, 92, 36,
	36, 93, 92,
	36, 93, 35,
	35, 94, 93,
	35, 94, 34,
	34, 95, 94,
	34, 95, 33,
	32, 95, 33,
	32, 95, 96,
	31, 96, 32,
	31, 96, 97,
	30, 97, 31,
	30, 97, 98,
	29, 98, 30,
	29, 98, 99,
	28, 99, 29,
	28, 99, 100,
	27, 100, 28,
	27, 100, 101,
	1, 53, 52,
	1, 53, 2,
	2, 54, 53,
	2, 54, 3,
	3, 55, 54,
	3, 55, 4,
	4, 56, 55,
	4, 56, 5,
	5, 57, 56,
	5, 57, 6,
	6, 58, 57,
	6, 58, 7,
	7, 59, 58,
	7, 59, 8,
	9, 59, 8,
	9, 59, 60,
	10, 60, 9,
	10, 60, 61,
	11, 61, 10,
	11, 61, 62,
	12, 62, 11,
	12, 62, 63,
	13, 63, 12,
	13, 63, 64,
	14,	64, 13,
	14, 64, 65,
	15, 65, 14,
	15, 65, 66,
	15, 66, 67,
	15, 67, 16,
	16, 67, 68,
	16, 68, 17,
	17, 69, 68,
	17, 69, 18,
	18, 70, 69,
	18, 70, 19,
	19, 71, 70,
	19, 71, 20,
	20, 72, 71,
	20, 72, 21,
	21, 73, 22,
	21, 73, 72,
	23, 73, 22,
	23, 73, 74,
	24, 74, 23,
	24, 74, 75, 
};

/*
 * Simple C++ Test Suite
 */

bool loadModelInfo(const std::string& fileName,
        std::vector<aam::ModelPathType>& modelPaths)
{
    std::ifstream stream(fileName.c_str());
    std::vector<std::string> strData;

    rule<> quotedString =
            confix_p('"', (*anychar_p)[push_back_a(strData)], '"');
    rule<> item = quotedString >> ',' >> *space_p >>
            quotedString;
    
    if (!stream)
    {
        return false;
    }

    while (!stream.eof())
    {
        std::string line;
        std::getline(stream, line);

        if (!line.empty())
        {
            bool result = parse(line.c_str(), item, space_p).full;

            if (!result || strData.size() != 2)
            {
                std::cout << "Bad format of line: " << line << std::endl;
            }
            else
            {
                aam::ModelPathType modelPath(strData[0], strData[1]);
                modelPaths.push_back(modelPath);
            }
            
            strData.clear();
        }
    }

    stream.close();
    
    return true;
}

void testTrainGray()
{
    std::cout << "aamestimatorTest testTrainGray" << std::endl;

    aam::AAMEstimator estimator;
    std::vector<aam::ModelPathType> modelPaths;
    std::vector<cv::Vec3i> triangles;

    for (int i = 0; i < TRIANGLES_COUNT; i++)
    {
        triangles.push_back(cv::Vec3i(trianglesData[i * 3],
                trianglesData[i * 3 + 1], trianglesData[i * 3 + 2]));
    }

	if (!loadModelInfo(AamToolboxPath + "data/frame_glasses_list.txt", modelPaths))
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainGray (aamestimatorTest) message=Can' load model info list" << std::endl;
        return;
    }
    else if (modelPaths.empty())
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainGray (aamestimatorTest) message=Empty model info list" << std::endl;
        return;
    }

    std::cout << "Found " << modelPaths.size() << " models." << std::endl <<
            "Start training..." << std::endl;

    try
    {
        aam::TrainOptions options;

        options.setPCACutThreshold(0.95);
        options.setGrayScale(true);
        options.setMultithreading(true);
        options.setAAMAlgorithm(aam::algorithm::conventional);
        options.setTriangles(triangles);

        estimator.setTrainOptions(options);
        estimator.train(modelPaths);
		estimator.save(AamToolboxPath + "data/aam_frame_glasses_test.xml");
    }
    catch (std::exception& e)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainGray (aamestimatorTest) message=Exception occured: " <<
                e.what() << std::endl;
    }
}

void testTrainColor()
{
    std::cout << "aamestimatorTest testTrainColor" << std::endl;

    aam::AAMEstimator estimator;
    std::vector<aam::ModelPathType> modelPaths;
    std::vector<cv::Vec3i> triangles;

    for (int i = 0; i < TRIANGLES_COUNT; i++)
    {
        triangles.push_back(cv::Vec3i(trianglesData[i * 3],
                trianglesData[i * 3 + 1], trianglesData[i * 3 + 2]));
    }

	if (!loadModelInfo(AamToolboxPath + "data/frame_glasses_list.txt", modelPaths))
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainColor (aamestimatorTest) message=Can' load model info list" << std::endl;
        return;
    }
    else if (modelPaths.empty())
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainColor (aamestimatorTest) message=Empty model info list" << std::endl;
        return;
    }

    std::cout << "Found " << modelPaths.size() << " models." << std::endl <<
            "Start training..." << std::endl;

    try
    {
        aam::TrainOptions options;

        options.setPCACutThreshold(0.95);
        options.setGrayScale(false);
        options.setMultithreading(true);
        options.setAAMAlgorithm(aam::algorithm::conventional);
        options.setTriangles(triangles);

        estimator.setTrainOptions(options);
        estimator.train(modelPaths);
		estimator.save(AamToolboxPath + "data/aam_frame_glasses_c_test.xml");
    }
    catch (std::exception& e)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainColor (aamestimatorTest) message=Exception occured: " <<
                e.what() << std::endl;
    }
}

void testTrainICGray()
{
    std::cout << "aamestimatorTest testTrainICGray" << std::endl;

    aam::AAMEstimator estimator;
    std::vector<aam::ModelPathType> modelPaths;
    std::vector<cv::Vec3i> triangles;

    for (int i = 0; i < TRIANGLES_COUNT; i++)
    {
        triangles.push_back(cv::Vec3i(trianglesData[i * 3],
                trianglesData[i * 3 + 1], trianglesData[i * 3 + 2]));
    }

	if (!loadModelInfo(AamToolboxPath + "data/frame_glasses_list.txt", modelPaths))
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainICGray (aamestimatorTest) message=Can' load model info list" << std::endl;
        return;
    }
    else if (modelPaths.empty())
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainICGray (aamestimatorTest) message=Empty model info list" << std::endl;
        return;
    }

    std::vector<aam::ModelPathType> tmp(modelPaths.begin(),
            modelPaths.begin() + 10);

    std::cout << "Found " << modelPaths.size() << " models." << std::endl <<
            "Start training..." << std::endl;

    try
    {
        aam::TrainOptions options;

        options.setPCACutThreshold(0.95);
        options.setGrayScale(true);
        options.setMultithreading(true);
        options.setAAMAlgorithm(aam::algorithm::inverseComposition);
        options.setTriangles(triangles);

        estimator.setTrainOptions(options);
        estimator.train(modelPaths);
		estimator.save(AamToolboxPath + "data/aam_frame_glasses_ic_test.xml");
    }
    catch (std::exception& e)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainICGray (aamestimatorTest) message=Exception occured: " <<
                e.what() << std::endl;
    }
}

void testTrainICColor()
{
    std::cout << "aamestimatorTest testTrainICColor" << std::endl;

    aam::AAMEstimator estimator;
    std::vector<aam::ModelPathType> modelPaths;
    std::vector<cv::Vec3i> triangles;

    for (int i = 0; i < TRIANGLES_COUNT; i++)
    {
        triangles.push_back(cv::Vec3i(trianglesData[i * 3],
                trianglesData[i * 3 + 1], trianglesData[i * 3 + 2]));
    }

	if (!loadModelInfo(AamToolboxPath + "data/frame_glasses_list.txt", modelPaths))
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainICColor (aamestimatorTest) message=Can' load model info list" << std::endl;
        return;
    }
    else if (modelPaths.empty())
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainICColor (aamestimatorTest) message=Empty model info list" << std::endl;
        return;
    }

    std::vector<aam::ModelPathType> tmp(modelPaths.begin(),
            modelPaths.begin() + 10);

    std::cout << "Found " << modelPaths.size() << " models." << std::endl <<
            "Start training..." << std::endl;

    try
    {
        aam::TrainOptions options;

        options.setPCACutThreshold(0.95);
        options.setGrayScale(false);
        options.setMultithreading(true);
        options.setAAMAlgorithm(aam::algorithm::inverseComposition);
        options.setTriangles(triangles);

        estimator.setTrainOptions(options);
        estimator.train(modelPaths);
		estimator.save(AamToolboxPath + "data/aam_frame_glasses_ic_c_test.xml");
    }
    catch (std::exception& e)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testTrainICColor (aamestimatorTest) message=Exception occured: " <<
                e.what() << std::endl;
    }
}

int main(int argc, char** argv)
{
	initAamToolboxPath();

    std::cout << "%SUITE_STARTING% aamestimatorTest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% testTrainGray (aamestimatorTest)" << std::endl;
    testTrainGray();
    std::cout << "%TEST_FINISHED% time=0 testTrainGray (aamestimatorTest)" << std::endl;

    std::cout << "%TEST_STARTED% testTrainColor (aamestimatorTest)" << std::endl;
    testTrainColor();
    std::cout << "%TEST_FINISHED% time=0 testTrainColor (aamestimatorTest)" << std::endl;

    std::cout << "%TEST_STARTED% testTrainICGray (aamestimatorTest)" << std::endl;
    testTrainICGray();
    std::cout << "%TEST_FINISHED% time=0 testTrainICGray (aamestimatorTest)" << std::endl;

    std::cout << "%TEST_STARTED% testTrainICColor (aamestimatorTest)" << std::endl;
    testTrainICColor();
    std::cout << "%TEST_FINISHED% time=0 testTrainICColor (aamestimatorTest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}
